    import http from 'k6/http';
    import { check } from 'k6';

    function sit() {
        let oldLkdDeposit = http.post('https://cash-in-sit-api.astrapay.com/cashin-service/topups',
            JSON.stringify({
                "amount": 1000000000000,
                "phoneNumber": "082113122229",
                "partnerCode": "LKD",
                "referenceNumber": new Date().getTime(),
                "channel": "CIMB",
                "userId": 219,
                "topupStatus": "COMMIT"
            }),
            {
                headers: { "Content-Type": "application/json" }
            })
        check(oldLkdDeposit,
            {
                "Response OK oldLkdDeposit": r => r.status === 201
            })
        console.log("request oldLkdDeposit: " + JSON.stringify(JSON.parse(oldLkdDeposit.request.body), null, 4))
        console.log("response oldLkdDeposit: " + JSON.stringify(JSON.parse(oldLkdDeposit.body), null, 4))

        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        let newLkdDepositInquiry = http.post('https://cash-in-sit-api.astrapay.com/cashin-service/topups',
            JSON.stringify({
                "accountIdDestination": 558,
                "accountIdSource": 22,
                "amount": 1000000000000,
                "referenceNumber": new Date().getTime(),
                "channel": "CIMB"
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdDepositInquiry,
            {
                "Response OK newLkdDepositInquiry": r => r.status === 201
            })
        console.log("request newLkdDepositInquiry: " + JSON.stringify(JSON.parse(newLkdDepositInquiry.request.body), null, 4))
        console.log("response newLkdDepositInquiry: " + JSON.stringify(JSON.parse(newLkdDepositInquiry.body), null, 4))

        let newLkdDepositCommit = http.put(`https://cash-in-sit-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdDepositInquiry.body).id}`,
            JSON.stringify({
                "status": "COMMIT",
                "amount": 1000000000000
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdDepositCommit,
            {
                "Response OK newLkdDepositCommit": r => r.status === 200
            })
        console.log(`request url: https://cash-in-sit-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdDepositInquiry.body).id}`)
        console.log("request newLkdDepositCommit: " + JSON.stringify(JSON.parse(newLkdDepositCommit.request.body), null, 4))
        console.log("response newLkdDepositCommit: " + JSON.stringify(JSON.parse(newLkdDepositCommit.body), null, 4))

        // ////////////////////////////////////////////////////////////////////////////////////////////////////////

        let newLkdCustomerInquiry = http.post('https://cash-in-sit-api.astrapay.com/cashin-service/topups',
            JSON.stringify({
                "accountIdDestination": 89,
                "accountIdSource": 558,
                "amount": 10000,
                "referenceNumber": new Date().getTime(),
                "channel": "CIMB"
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdCustomerInquiry,
            {
                "Response OK newLkdCustomerInquiry": r => r.status === 201
            })
        console.log("request newLkdCustomerInquiry: " + JSON.stringify(JSON.parse(newLkdCustomerInquiry.request.body), null, 4))
        console.log("response newLkdCustomerInquiry: " + JSON.stringify(JSON.parse(newLkdCustomerInquiry.body), null, 4))

        let newLkdCustomerCommit = http.put(`https://cash-in-sit-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdCustomerInquiry.body).id}`,
            JSON.stringify({
                "status": "COMMIT",
                "amount": 10000
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdCustomerCommit,
            {
                "Response OK newLkdCustomerCommit": r => r.status === 200
            })
        console.log(`request url: https://cash-in-sit-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdCustomerInquiry.body).id}`)
        console.log("request newLkdCustomerCommit: " + JSON.stringify(JSON.parse(newLkdCustomerCommit.request.body), null, 4))
        console.log("response newLkdCustomerCommit: " + JSON.stringify(JSON.parse(newLkdCustomerCommit.body), null, 4))
    }

    function uat() {
        let oldLkdDeposit = http.post('https://cash-in-uat-api.astrapay.com/cashin-service/topups',
            JSON.stringify({
                "amount": 1000000000000,
                "phoneNumber": "08772380asd0002",
                "partnerCode": "LKD",
                "referenceNumber": new Date().getTime(),
                "channel": "CIMB",
                "userId": 21623423435,
                "topupStatus": "COMMIT"
            }),
            {
                headers: { "Content-Type": "application/json" }
            })
        check(oldLkdDeposit,
            {
                "Response OK oldLkdDeposit": r => r.status === 201
            })
        console.log("request oldLkdDeposit: " + JSON.stringify(JSON.parse(oldLkdDeposit.request.body), null, 4))
        console.log("response oldLkdDeposit: " + JSON.stringify(JSON.parse(oldLkdDeposit.body), null, 4))

        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        let newLkdDepositInquiry = http.post('https://cash-in-uat-api.astrapay.com/cashin-service/topups',
            JSON.stringify({
                "accountIdDestination": 757,
                "accountIdSource": 22,
                "amount": 1000000000000,
                "referenceNumber": new Date().getTime(),
                "channel": "CIMB"
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdDepositInquiry,
            {
                "Response OK newLkdDepositInquiry": r => r.status === 201
            })
        console.log("request newLkdDepositInquiry: " + JSON.stringify(JSON.parse(newLkdDepositInquiry.request.body), null, 4))
        console.log("response newLkdDepositInquiry: " + JSON.stringify(JSON.parse(newLkdDepositInquiry.body), null, 4))

        let newLkdDepositCommit = http.put(`https://cash-in-uat-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdDepositInquiry.body).id}`,
            JSON.stringify({
                "status": "COMMIT",
                "amount": 1000000000000
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdDepositCommit,
            {
                "Response OK newLkdDepositCommit": r => r.status === 200
            })
        console.log(`request url: https://cash-in-uat-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdDepositInquiry.body).id}`)
        console.log("request newLkdDepositCommit: " + JSON.stringify(JSON.parse(newLkdDepositCommit.request.body), null, 4))
        console.log("response newLkdDepositCommit: " + JSON.stringify(JSON.parse(newLkdDepositCommit.body), null, 4))

        // ////////////////////////////////////////////////////////////////////////////////////////////////////////

        let newLkdCustomerInquiry = http.post('https://cash-in-uat-api.astrapay.com/cashin-service/topups',
            JSON.stringify({
                "accountIdDestination": 243,
                "accountIdSource": 757,
                "amount": 10000,
                "referenceNumber": new Date().getTime(),
                "channel": "CIMB"
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdCustomerInquiry,
            {
                "Response OK newLkdCustomerInquiry": r => r.status === 201
            })
        console.log("request newLkdCustomerInquiry: " + JSON.stringify(JSON.parse(newLkdCustomerInquiry.request.body), null, 4))
        console.log("response newLkdCustomerInquiry: " + JSON.stringify(JSON.parse(newLkdCustomerInquiry.body), null, 4))

        let newLkdCustomerCommit = http.put(`https://cash-in-uat-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdCustomerInquiry.body).id}`,
            JSON.stringify({
                "status": "COMMIT",
                "amount": 10000
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdCustomerCommit,
            {
                "Response OK newLkdCustomerCommit": r => r.status === 200
            })
        console.log(`request url: https://cash-in-uat-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdCustomerInquiry.body).id}`)
        console.log("request newLkdCustomerCommit: " + JSON.stringify(JSON.parse(newLkdCustomerCommit.request.body), null, 4))
        console.log("response newLkdCustomerCommit: " + JSON.stringify(JSON.parse(newLkdCustomerCommit.body), null, 4))
    }

    export default function () {
        let oldLkdDeposit = http.post('https://cash-in-sit-api.astrapay.com/cashin-service/topups',
            JSON.stringify({
                "amount": 1000000000000,
                "phoneNumber": "082113122229",
                "partnerCode": "LKD",
                "referenceNumber": new Date().getTime(),
                "channel": "CIMB",
                "userId": 219,
                "topupStatus": "COMMIT"
            }),
            {
                headers: { "Content-Type": "application/json" }
            })
        check(oldLkdDeposit,
            {
                "Response OK oldLkdDeposit": r => r.status === 201
            })
        console.log("request oldLkdDeposit: " + JSON.stringify(JSON.parse(oldLkdDeposit.request.body), null, 4))
        console.log("response oldLkdDeposit: " + JSON.stringify(JSON.parse(oldLkdDeposit.body), null, 4))

        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        let newLkdDepositInquiry = http.post('https://cash-in-sit-api.astrapay.com/cashin-service/topups',
            JSON.stringify({
                "accountIdDestination": 558,
                "accountIdSource": 22,
                "amount": 1000000000000,
                "referenceNumber": new Date().getTime(),
                "channel": "CIMB"
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdDepositInquiry,
            {
                "Response OK newLkdDepositInquiry": r => r.status === 201
            })
        console.log("request newLkdDepositInquiry: " + JSON.stringify(JSON.parse(newLkdDepositInquiry.request.body), null, 4))
        console.log("response newLkdDepositInquiry: " + JSON.stringify(JSON.parse(newLkdDepositInquiry.body), null, 4))

        let newLkdDepositCommit = http.put(`https://cash-in-sit-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdDepositInquiry.body).id}`,
            JSON.stringify({
                "status": "COMMIT",
                "amount": 1000000000000
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdDepositCommit,
            {
                "Response OK newLkdDepositCommit": r => r.status === 200
            })
        console.log(`request url: https://cash-in-sit-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdDepositInquiry.body).id}`)
        console.log("request newLkdDepositCommit: " + JSON.stringify(JSON.parse(newLkdDepositCommit.request.body), null, 4))
        console.log("response newLkdDepositCommit: " + JSON.stringify(JSON.parse(newLkdDepositCommit.body), null, 4))

        // ////////////////////////////////////////////////////////////////////////////////////////////////////////

        let newLkdCustomerInquiry = http.post('https://cash-in-sit-api.astrapay.com/cashin-service/topups',
            JSON.stringify({
                "accountIdDestination": 90,
                "accountIdSource": 558,
                "amount": 10000,
                "referenceNumber": new Date().getTime(),
                "channel": "CIMB"
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdCustomerInquiry,
            {
                "Response OK newLkdCustomerInquiry": r => r.status === 201
            })
        console.log("request newLkdCustomerInquiry: " + JSON.stringify(JSON.parse(newLkdCustomerInquiry.request.body), null, 4))
        console.log("response newLkdCustomerInquiry: " + JSON.stringify(JSON.parse(newLkdCustomerInquiry.body), null, 4))

        let newLkdCustomerCommit = http.put(`https://cash-in-sit-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdCustomerInquiry.body).id}`,
            JSON.stringify({
                "status": "COMMIT",
                "amount": 10000
            }),
            {
                headers: {
                    "Content-Type": "application/json",
                    "x-partner-code": "LKD",
                    "x-timestamp": "2022-01-01T00:00:00.000000"
                }
            })
        check(newLkdCustomerCommit,
            {
                "Response OK newLkdCustomerCommit": r => r.status === 200
            })
        console.log(`request url: https://cash-in-sit-api.astrapay.com/cashin-service/topups/${JSON.parse(newLkdCustomerInquiry.body).id}`)
        console.log("request newLkdCustomerCommit: " + JSON.stringify(JSON.parse(newLkdCustomerCommit.request.body), null, 4))
        console.log("response newLkdCustomerCommit: " + JSON.stringify(JSON.parse(newLkdCustomerCommit.body), null, 4))
    }
